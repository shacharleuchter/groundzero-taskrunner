#!/usr/bin/env node
'use strict';

const minimist = require('minimist')

const args = minimist(process.argv.slice(2))

let cmd = args._[0] || 'help'

if (args.version || args.v) {
    cmd = 'version'
}

if (args.help || args.h) {
    cmd = 'help'
}

switch (cmd) {
    case 'dev':
        const copyAssets = require('./lib/copy-assets')
        const copyIcons = require('./lib/copy-icons')
        const buildIcons = require('./lib/build-icons')
        const buildCss = require('./lib/build-css')
        const watchCss = require('./lib/watch-css')
        const watchIcons = require('./lib/watch-icons')
        const watchJs = require('./lib/watch-js')
        const fractalStart = require('./lib/fractal-start')
        
        copyAssets({})
        copyIcons({})
        buildIcons({})
        buildCss({})
        buildCss({ styleguide: true }).then(() => {
            // watch scripts
            // watchCss()
            // watchJs()
            // watchIcons()
            fractalStart()
        })

        break

    case 'build':
        require('./lib/build')(args)
        break
    
    case 'release':
        require('./lib/release')(args)
        break

    case 'deploy':
        require('./lib/deploy')(args)
        break

    case 'newpattern':
        require('./lib/newpattern')(args)
        break

    case 'version':
        require('./lib/version')(args)
        break

    case 'help':
        require('./lib/help')(args)
        break

    default:
        error(`"${cmd}" is not a valid command!`, true)
        break
}