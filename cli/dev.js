#!/usr/bin/env node
'use strict';

const runAll = require("npm-run-all");

console.log("running dev");

runAll(["dev"], {parallel: false})
    .then(() => {
        console.log("running dev done!");
    })
    .catch(err => {
        console.log("running dev failed!", err);
    });