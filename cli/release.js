#!/usr/bin/env node
'use strict';

const runAll = require("npm-run-all");

console.log("running release");

runAll(["release"], {parallel: false})
    .then(() => {
        console.log("running release done!");
    })
    .catch(err => {
        console.log("running release failed!", err);
    });