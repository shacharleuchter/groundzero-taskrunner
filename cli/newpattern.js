#!/usr/bin/env node
'use strict';

const runAll = require("npm-run-all");

console.log("running newpattern");

runAll(["newpattern"], {parallel: false})
    .then(() => {
        console.log("running newpattern done!");
    })
    .catch(err => {
        console.log("running newpattern failed!", err);
    });