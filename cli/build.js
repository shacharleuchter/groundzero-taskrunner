#!/usr/bin/env node
'use strict';

const runAll = require("npm-run-all");

console.log("running build");

runAll(["build"], {parallel: false})
    .then(() => {
        console.log("running build done!");
    })
    .catch(err => {
        console.log("running build failed!", err);
    });