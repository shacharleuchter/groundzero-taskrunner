#!/usr/bin/env node
'use strict';

const runAll = require("npm-run-all");

console.log("running deploy");

runAll(["deploy"], {parallel: false})
    .then(() => {
        console.log("running deploy done!");
    })
    .catch(err => {
        console.log("running deploy failed!", err);
    });