# Jung von Matt/Neckar - GroundZero Taskrunner

## What is this script doing?

In charge of running the npm commands for groundzero projects by providing a CLI

## How is it doing it?

## Installation
1) npm install:
    ```
    npm install @jvmn/groundzero-taskrunner --save-dev
    ```

2) add a script to the scripts section in your package.json
    ```json
    "deploy": "groundzero:deploy",
    ``` 
    or any other hook you want to run it from.

## Options