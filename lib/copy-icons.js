const path = require('path');
const fractal = require(path.join(process.cwd(), '/fractal-config.js'));
const copydir = require('copy-dir');
const argv = require('yargs').argv;
const assetsPath = fractal.components.get('path') + '/assets/img/sprite-icons/';
const dest = argv.release ? fractal.get('project.release') + '/assets/img/sprite-icons/' : fractal.web.get('static.path') + '/assets/img/sprite-icons/';
const ora = require('ora')
/**
 * @ CLI Arguments
 * @param -release = output path release
 */

/**
 * Copy assets directory from develop/assets, ignore js folder that complies via webpack
 */

module.exports = async (args) => {
    const spinner = ora().start()

    try {
        await copydir(assetsPath, dest, function (stat, filepath, filename) {
            if (stat === 'directory' && filename === 'js') {
                return false;
            }
            return true;
        },function (err) {
            if (err) {
                console.log(err);
            } else {
                return console.log('assets folder copied');
            }
        });
        spinner.stop()

        return
    } catch (err) {
        spinner.stop()

        console.error(err)
    }
}
