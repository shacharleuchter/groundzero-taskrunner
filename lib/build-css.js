const globby = require('globby');
const concat = require('concat');
const path = require('path');
const fractal = require(path.join(process.cwd(), '/fractal-config.js'));
const fs = require('fs');
const sass = require('node-sass');
const orderBy = require('natural-orderby');
const autoprefixer = require('autoprefixer');
const postcss = require('postcss');
const cssnano = require('cssnano');
const ora = require('ora')
// const argv = require('yargs').argv;

const globals = fractal.components.get('path') + '/00-globals/**/*.scss';
const components = fractal.components.get('path') + '/**/*.scss';
const styleguide = fractal.components.get('path') + '/**/*.styleguide.scss'

async function getScssFiles(args) {
    try {
        const paths = args.styleguide ? [globals, styleguide] : [components, '!' + styleguide];
        args.styleguide ? console.log('fetched all styleguide files') : console.log('fetched all style files')
        const files = await globby(paths);
        const sortedFiles = await orderBy.orderBy(files);
        return sortedFiles;
    } catch (err) {
        console.error(err)
    }
}

async function concatScss(files) {
    try {
        return await concat(files).then(result => {
            console.log('merged all scss files', true)
            return result
        })
    } catch (err) {
        console.error(err)
    }
}

function convertToCss(file) {
    return new Promise(function (resolve, reject) {
        try {
            sass.render({ data: file, sourceMap: true, outFile: 'web-ui/assets/css/' + outputFileName + '.css' }, function (err, result) {
                console.log('converted sass to css')
                // return result
                resolve(result)
            })
        } catch (e) {
            reject(e)
        }
    })
}

function postCss() {
    return new Promise(function (resolve, reject) {
        try {
            fs.readFile(outputPath + '.css', (err, css) => {
                postcss([autoprefixer])
                    .process(css, { from: outputPath + '.css', to: outputPath + '.css', map: { inline: false } })
                    .then(result => {
                        console.log('postcss with autoprifix finished')
                        console.log(outputFileName + '.css.map generated')
                        fs.writeFile(outputPath + '.css', result.css, () => resolve())
                        if (result.map) {
                            fs.writeFile(outputPath + '.css.map', result.map, () => true)
                        }
                    })
            })
        } catch (e) {
            reject(e)
        }
    })
}

function minifyCss() {
    return new Promise(function (resolve, reject) {
        try {
            fs.readFile(outputPath + '.css', (err, css) => {
                postcss([cssnano])
                    .process(css, { from: outputPath + '.css', to: outputPath + '.min.css', map: { inline: false } })
                    .then(result => {
                        console.log('minify finished')
                        console.log(outputFileName + '.min.css and .min.map generated')
                        fs.writeFile(outputPath + '.min.css', result.css, () => resolve())
                        if (result.map) {
                            fs.writeFile(outputPath + '.min.map', result.map, () => true)
                        }
                    })
            })
        } catch (e) {
            reject(e)
        }
    })
}

function saveCss(file) {
    return new Promise(function (resolve, reject) {
        try {
            fs.writeFile(
                outputPath + '.css',
                file.css.toString(),
                'utf8',
                function (errWritingOptimized) {
                    if (errWritingOptimized) reject(errWritingOptimized)
                    console.log(outputFileName + '.css file saved successfully')
                    resolve()
                }
            )
        } catch (e) {
            reject(e)
        }
    })
}

module.exports = async (args) => {
    const spinner = ora().start()

    try {
        const outputDir = args.release ? path.join(process.cwd(), '/release/assets/css/') : path.join(process.cwd(), '/web-ui/assets/css/');
        const outputFileName = args.styleguide ? 'styleguide' : 'global';
        const outputPath = outputDir + outputFileName;

        const scssFiles = await getScssFiles(args)
        const scssFile = await concatScss(scssFiles)
        const cssFile = await saveCss(scssFile)
        
        await postCss()
        if(args.minify || args.m) await minifyCss()
        spinner.stop()

        return
    } catch (err) {
        spinner.stop()

        console.error(err)
    }
}