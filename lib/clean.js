const del = require( 'del' );
const path = require('path');
const fractal = require(path.join(process.cwd(), '/fractal-config.js'));
const argv = require('yargs').argv;

let dest = undefined;

if (argv.ui) {
    dest = [fractal.web.get('static.path')]
} else if (argv.build) {
    dest = [fractal.web.get('builder.dest')]
} else if (argv.release) {
    dest = [fractal.get('project.release')]
}

/**
 * delets a directory
 */
try {
    del(dest).then(function() {
        return console.log(dest + ' deleted')
    });
} catch (e) {
    console.log(e)
}
