const chokidar = require('chokidar')
const buildIcons = require('./build-icons')

// Initialize watcher.
const watcher = chokidar.watch('develop/assets/img/sprite-icons/*.svg', {
    ignored: /(^|[\/\\])\../,
    persistent: true
});

watcher.on('all', (event, path) => {
    console.log(event, path);
    buildIcons();
    // if (path.indexOf('.styleguide.scss') !== -1) {
    //     buildCss({ styleguide: true })
    // } else {
    //     buildCss({ styleguide: false })
    // }
});