const webpack = require("webpack");
const path = require('path');
const config = require(path.join(process.cwd(), '/webpack.dev.js'));

const compiler = webpack(config);

const watching = compiler.watch({
    // https://webpack.js.org/api/node/
    // Example watchOptions
    aggregateTimeout: 300,
    poll: undefined
}, (err, stats) => {
    // Print watch/build result here...
    console.log(stats.toString({
        chunks: false,  // Makes the build much quieter
        colors: true    // Shows colors in the console
    }));
});