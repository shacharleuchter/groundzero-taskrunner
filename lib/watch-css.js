const chokidar = require('chokidar')
const buildCss = require('./build-css')

// Initialize watcher.
const watcher = chokidar.watch('develop/**/**/*.scss', {
    ignored: /(^|[\/\\])\../,
    persistent: true
});

watcher.on('all', (event, path) => {
    console.log(event, path);
    if (path.indexOf('.styleguide.scss') !== -1) {
        buildCss({ styleguide: true })
    } else {
        buildCss({ styleguide: false })
    }
});