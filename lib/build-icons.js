const ora = require('ora');
// const exec = require('child_process').exec;
const util = require('util');
const exec = util.promisify(require('child_process').exec);
process.env.PROJECT_CWD = process.env.INIT_CWD;
// https://zaiste.net/nodejs-child-process-spawn-exec-fork-async-await/
// https://nodejs.org/api/child_process.html#child_process_child_process_exec_command_options_callback
module.exports = async (args) => {
    const spinner = ora().start()

    try {
        // const child = exec('svg-sprite --config svg-sprite.config.js -D web-ui/assets/img/sprite-icons develop/assets/img/sprite-icons/*.svg',
        // (error, stdout, stderr) => {
        //     console.log(`stdout: ${stdout}`);
        //     console.log(`stderr: ${stderr}`);
        //     if (error !== null) {
        //         console.log(`exec error: ${error}`);
        //     }
        //     });
        const { stdout, stderr } = await exec('svg-sprite --config svg-sprite.config.js -D web-ui/assets/img/sprite-icons develop/assets/img/sprite-icons/*.svg', {
            env: process.env,
        });
        console.log('stdout:', stdout);
        console.log('stderr:', stderr);
        spinner.stop()
    } catch (err) {
        spinner.stop()

        console.error(err)
    }
}
