const menus = {
    main: `
      groundzero [command] <options>
  
      dev .............. start fractal on dev mode
      build ........... build fractal standalone for deploying
      release ............ make a release
      deploy ............ build & deploy fractal via ssh
      newpattern ............... new moudle / component boilerplate,
      version ............ show package version
      help ............... show help menu for a command`,

    deploy: `
      groundzero deploy <options>
        
      --build, -b ..... build before deploying,
      --dev, -d ..... deploy to dev enviorment,
      --stage, -s ..... deploy to stage enviorment,
      --all, -a ..... deploy to dev and stage enviorment,
      --feature, -f ..... deploy a feature brance combine it with an enviorment flag`,
}

module.exports = (args) => {
    const subCmd = args._[0] === 'help'
        ? args._[1]
        : args._[0]

    console.log(menus[subCmd] || menus.main)
}